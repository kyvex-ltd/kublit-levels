# Kublit levels

This is the repository for community levels for the game [Kublit](https://kyvex-ltd.gitlab.io/kublit/).

## How can I contribute a level?

### Create a level

1. Fork this repository
2. Create a new `.json` file in the `levels` folder. See [example.json](levels/example.json) for the format.
3. Add a preview image to the `img` folder. The image should be ideally 428x428 pixels, but as long as it is square, 
   it should be fine.
4. Add your level to the `dataset.json` file
   ```json
    {
      "name": "My level",
      "file": "my-level.json",
      "image": "my-level.png"
    }
    ```
5. Create a merge request

### How can I create a level?

- You can either use the [level editor (soon)](https://kyvex-ltd.gitlab.io/kublit/level-editor/) to create a level.
- or, you can create a level by hand. See [example.json](levels/example.json) for the format.

## How can I play the levels?

Visit https://kyvex-ltd.gitlab.io/kublit/levels/ to play the levels.